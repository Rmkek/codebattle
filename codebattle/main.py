from flask import Flask, request, jsonify, session
from codebattle.database import User, Room, UserInRoom, TaskTest, Task, UserSolvedTask  # noqa:501
from codebattle.tasks import get_random_task
from codebattle.util import allowed_file
from werkzeug.utils import secure_filename
from subprocess import call
import os
import datetime
import uuid

app = Flask(__name__)

UPLOAD_FOLDER = 'downloaded_code'
ALLOWED_EXTENSIONS = set(['cpp', 'c', 'py'])

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = \
    b'ddc4939b0a6288a79c4eaad7977f13a09974bbe7cb14e979a1be896bf7fce73a'


@app.route("/api/login", methods=['POST'])
def login():
    login = request.json.get('login')
    password = request.json.get('password')

    query = User.select().where(
        User.login == login,
        User.password == password
    )

    if len(query) == 1:
        session['login'] = request.json.get('login')
        session['password'] = request.json.get('password')

        return jsonify({'success': True}), 200
    else:
        return jsonify({
            'success': False,
            'error': 'Wrong password or login'
            }), 401


@app.route('/api/logout')
def logout():
    session.pop('username', None)
    session.pop('password', None)
    session.pop('room_id', None)

    return jsonify({'success': True}), 200


@app.route("/api/create_room", methods=['POST'])
def create_room():
    login = session['login']
    password = session['password']

    user = User.select().where(User.login == login,
                               User.password == password)[0]

    room_id = uuid.uuid1()

    Room(is_open=True,
         task=get_random_task(),
         created=datetime.datetime.now(),
         room_connection_id=room_id).save()

    room = Room.select().where(Room.room_connection_id == room_id).get()

    UserInRoom(user=user, room=room).save()

    session['room_id'] = room_id

    return jsonify({
        'success': True,
        'room_id': room_id
        }), 200


@app.route('/api/join_room', methods=['POST'])
def join_room():
    login = session['login']
    password = session['password']
    room_id = request.json.get('room_id')

    query = UserInRoom.select().join(Room) \
                      .where(Room.room_connection_id == room_id, Room.is_open)

    if not query:
        return jsonify({
            'success': False,
            'error': 'Room does not exist'
            }), 404

    if len(query) == 2:
        return jsonify({
            'success': False,
            'error': 'Room is full'
            }), 400
    elif query[0].user.login == login:
        return jsonify({
            'success': False,
            'error': 'User is already in this room',
            }), 403

    user = User.select().where(User.login == login,
                               User.password == password).get()
    UserInRoom(user=user, room=Room.select()
               .where(Room.room_connection_id == room_id).get()).save()

    query[0].room.is_open = False
    query[0].room.save()

    session['room_id'] = room_id

    return jsonify({
        'success': True,
        'task_description': query[0].room.task.description
        }), 200


@app.route('/api/ping_room', methods=['POST'])
def ping_room():
    room_id = session['room_id']

    query = UserInRoom.select().join(Room) \
                      .where(Room.room_connection_id == room_id)

    if len(query) == 2:
        return jsonify({
            'success': True,
            'task_description': query[0].room.task.description
            }), 200

    return jsonify({'success': False, 'error': 'Room is not full'}), 404


@app.route('/api/close_room', methods=['POST'])
def close_room():
    room_id = session['room_id']

    query = UserInRoom.select().join(Room) \
                      .where(Room.room_connection_id == room_id)

    for each in query:
        each.delete_instance()

    room_to_be_closed = Room.delete().where(Room.room_connection_id == room_id)
    room_to_be_closed.execute()

    return jsonify({'success': True, 'room_id': room_id})


@app.route('/api/submit_code', methods=['POST'])
def submit_code():
    if request.method == 'POST':
        if 'code' not in request.files:
            return jsonify({'success': False,
                            'error': 'No code file supplied'}), 400

        file = request.files['code']
        if file and allowed_file(file.filename, ALLOWED_EXTENSIONS):
            file_extension = file.filename.split('.')[-1]
            filename = secure_filename(
                f'{session["login"]}.{file_extension}')
            filename_without_extension = session['login']
            local_file = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            if os.getcwd().split('/')[-1] == app.config['UPLOAD_FOLDER']:
                os.chdir(os.path.dirname(os.getcwd()))

            file.save(local_file)

            user_room = UserInRoom.select().join(Room) \
                                  .where(Room.room_connection_id
                                         == session['room_id']).get()

            if user_room.room.is_open:
                return jsonify({'success': False,
                                'error': 'Second user has not joined the room'
                                }), 403

            tasks = TaskTest.select().join(Task).where(Task.id
                                                       == user_room.room.task)

            test_number = 1
            print(len(tasks))

            for each in tasks:
                cwd = os.getcwd()
                if cwd.split('/')[-1] != app.config['UPLOAD_FOLDER']:
                    directory_path = os.path.join(cwd,
                                                  app.config['UPLOAD_FOLDER'])
                    os.chdir(directory_path)
                call(f'echo "{each.test_input}" | python {filename} 1> {filename_without_extension}.out 2> {filename_without_extension}.err', shell=True)  # noqa: E501

                with open(f'{filename_without_extension}.err', 'r') as errors:
                    read = errors.read().strip()
                    print(f'Errors: ', read)
                    if read:
                        print(f'Got some errors: {read}')
                        return jsonify({'success': False,
                                        'error': f'Exception: {read}'}), 400

                with open(f'{filename_without_extension}.out', 'r') as output:
                    read = output.read().strip()
                    if read != each.test_output:
                        return jsonify({'success': False,
                                        'error': f'Tests failed at test #{test_number}'}), 400  # noqa:501
                test_number += 1

            os.remove(filename)
            os.remove(f'{filename_without_extension}.out')
            os.remove(f'{filename_without_extension}.err')

            os.chdir(os.path.dirname(os.getcwd()))

            # первый - победитель

            user_db = User.select().where(User.login == session['login'],
                                          User.password
                                          == session['password']).get()
            query = UserSolvedTask.select().join(Room) \
                .where(Room.room_connection_id == session['room_id'])

            print(query)
            print(len(query))

            UserSolvedTask(user=user_db,
                           task=tasks[0].task,
                           room=user_room.room,
                           solved_time=datetime.datetime.now()).save()

            print('All tests succesfully passed.')
            return jsonify({'success': True,
                            'result': 'Winner' if not query else 'Loser'}), 200


@app.route('/api/get_competition_results', methods=['POST'])
def get_competition_results():
    room_id = session['room_id']
    query = UserSolvedTask.select().join(Room) \
                          .where(Room.room_connection_id == room_id)

    if len(query) == 1:
        return jsonify({'success': False,
                        'error': 'Second user has not yet solved the task'}),\
                        404
    first_user_solvetime = query[0].solved_time
    second_user_solvetime = query[1].solved_time

    if first_user_solvetime > second_user_solvetime:
        return jsonify({'success': True, 'winner': query[1].user.login})
    else:
        return jsonify({'success': True, 'winner': query[0].user.login})
