from peewee import SqliteDatabase, CharField, Model, DateField, \
    BooleanField, ForeignKeyField, UUIDField

db = SqliteDatabase('users.db')


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    login = CharField(unique=True)
    password = CharField()


class Task(BaseModel):
    description = CharField()


class Room(BaseModel):
    is_open = BooleanField()
    room_connection_id = UUIDField()
    created = DateField()
    task = ForeignKeyField(Task)


class UserInRoom(BaseModel):
    user = ForeignKeyField(User)
    room = ForeignKeyField(Room)


class UserSolvedTask(BaseModel):
    user = ForeignKeyField(User)
    task = ForeignKeyField(Task)
    room = ForeignKeyField(Room)
    solved_time = DateField()


class TaskTest(BaseModel):
    test_input = CharField()
    test_output = CharField()
    task = ForeignKeyField(Task)


if __name__ == '__main__':
    db.create_tables([User, Room, UserInRoom, Task, TaskTest, UserSolvedTask])

    romka = User(login='Romka', password="Romka1337")
    romka.save()

    vladik = User(login='Vladik', password='Vladik1337')
    vladik.save()
