import random
from codebattle.database import Task, TaskTest


def get_random_task():
    return random.choice(Task.select())


if __name__ == '__main__':
    print('Creating tasks in DB.')

    basketballTask = Task(description='Известны результаты каждой из 4х четвертей баскетбольной встречи. Нужно определить победителя матча. Побеждает команда, набравшая больше очков в течение всего матча.\nВыведите номер выигравшей команды, в случае ничьей следует вывести «DRAW».').save()  # noqa: E501

    firstTest = TaskTest(task=basketballTask,
                         test_input="26 17\n13 15\n19 11\n14 16",
                         test_output="1").save()

    secondTest = TaskTest(task=basketballTask,
                          test_input="14 15\n17 18\n20 20\n15 17",
                          test_output="2").save()

    thirdTest = TaskTest(task=basketballTask,
                         test_input="15 16\n18 17\n10 12\n14 12",
                         test_output="DRAW").save()
